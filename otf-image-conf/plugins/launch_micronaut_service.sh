#!/bin/bash

function proxy_host() {
    echo "$(echo $1 | awk -F[/:] '{if ($1 ~ /^http/) print $4; else print $1}')"
}
function proxy_port() {
    echo "$(echo $1 | awk -v DEFAULT=$2 -F[/:] '{if ($1 ~ /^http/) if ($5) print $5; else print DEFAULT; else if ($2) print $2; else print DEFAULT}')"
}

TAR_NAME="$1"
PLUGIN_PATH="$2"
CONFIG_FILE_COMMAND="$3"

DEBUG_LEVEL_ARG="${DEBUG_LEVEL:-INFO}"
BUS_HOST_ARG="${BUS_HOST:-127.0.0.1}"
BUS_PORT_ARG="${BUS_PORT:-38368}"
EXTERNAL_HOSTNAME_ARG="${EXTERNAL_HOSTNAME:-127.0.0.1}"
TRUST_FILES_ARG="${TRUST_FILES:-/}"

if [ "$DEBUG_LEVEL_ARG" == "CRITICAL" ]; then DEBUG_LEVEL_ARG="ERROR"; fi;
if [ "$DEBUG_LEVEL_ARG" == "WARNING" ]; then DEBUG_LEVEL_ARG="WARN"; fi;
if [ "$DEBUG_LEVEL_ARG" == "NOTSET" ]; then DEBUG_LEVEL_ARG="TRACE"; fi;
BASE_COMMAND="-Dorg.opentestfactory.insecure=true -Dorg.opentestfactory.auth.trustedAuthorities=$TRUST_FILES_ARG -Dorg.opentestfactory.bus.baseUrl=http://$BUS_HOST_ARG:$BUS_PORT_ARG -Dorg.opentestfactory.tf2.external-hostname=$EXTERNAL_HOSTNAME_ARG -Dlogger.levels.org.opentestfactory=$DEBUG_LEVEL_ARG"
if [ -z "$BUS_TOKEN" ]; then OPTIONAL_COMMAND=""; else  OPTIONAL_COMMAND="-Dorg.opentestfactory.bus.authToken=$BUS_TOKEN"; fi;

if [ -z "${NO_PROXY}" ]
then
    NOPROXY_ARG=""
else
    NOPROXY_ARG=" -Dhttp.nonProxyHosts='$(echo $NO_PROXY | sed 's/,/|/g')'"
fi
if [ -z "${HTTPS_PROXY}" ]
then
    HTTPSPROXY_ARG=""
else
    HTTPSPROXY_ARG=" -Dhttps.proxyHost=$(proxy_host $HTTPS_PROXY) -Dhttps.proxyPort=$(proxy_port $HTTPS_PROXY 443)"
fi
if [ -z "${HTTP_PROXY}" ]
then
    HTTPPROXY_ARG=""
else
    HTTPPROXY_ARG=" -Dhttp.proxyHost=$(proxy_host $HTTP_PROXY) -Dhttp.proxyPort=$(proxy_port $HTTP_PROXY 80)"
fi
PROXY_COMMAND="${HTTPSPROXY_ARG}${HTTPPROXY_ARG}${NOPROXY_ARG}"


/bin/bash -c "tar -xzf "/app/plugins/java/${TAR_NAME}" -C /app/plugins/java/"
/bin/bash -c "/app/plugins/java/${PLUGIN_PATH} ${BASE_COMMAND} ${OPTIONAL_COMMAND} ${PROXY_COMMAND} ${CONFIG_FILE_COMMAND}"
