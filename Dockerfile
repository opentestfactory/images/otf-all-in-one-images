## Retrieve maven components
FROM maven:3.9.9-eclipse-temurin-21 AS java-retriever

ARG JAVA_PLUGINS_VERSION
ARG ALLURE_VERSION
ARG RAW_REPO_USERNAME
ARG RAW_REPO_PASSWORD
ARG RAW_GITHUB_PROXY
ARG OTELCOL_VERSION

COPY .m2/settings.xml /root/.m2/settings.xml

RUN sh -xe -c "mkdir -p /java-services/ ;\
            mvn dependency:copy -s /root/.m2/settings.xml -Dartifact=org.opentestfactory:otf-allure-report-collector:${JAVA_PLUGINS_VERSION}:tar.gz:native-image -Dtransitive=false -DoutputDirectory=/java-services/ -Dmdep.useBaseVersion -Dmdep.stripClassifier ;\
            mvn dependency:copy -s /root/.m2/settings.xml -Dartifact=org.opentestfactory:otf-result-aggregator:${JAVA_PLUGINS_VERSION}:tar.gz:native-image -Dtransitive=false -DoutputDirectory=/java-services/ -Dmdep.useBaseVersion -Dmdep.stripClassifier ;\
            mvn dependency:copy -s /root/.m2/settings.xml -Dartifact=org.opentestfactory:otf-interpreter:${JAVA_PLUGINS_VERSION}:tar.gz:native-image -Dtransitive=false -DoutputDirectory=/java-services/ -Dmdep.useBaseVersion -Dmdep.stripClassifier ;\
            apt update; apt install -y unzip;\
            cd /opt;mvn dependency:copy -s /root/.m2/settings.xml -Dartifact=io.qameta.allure:allure-commandline:${ALLURE_VERSION}:zip -DoutputDirectory=/opt;\
            unzip /opt/allure-commandline-${ALLURE_VERSION}.zip -d /opt"

RUN wget --https-only --secure-protocol=TLSv1_2 --user=${RAW_REPO_USERNAME} --password=${RAW_REPO_PASSWORD} ${RAW_GITHUB_PROXY}/open-telemetry/opentelemetry-collector-releases/releases/download/v${OTELCOL_VERSION}/otelcol_${OTELCOL_VERSION}_linux_amd64.tar.gz \
    && tar -xvf otelcol_${OTELCOL_VERSION}_linux_amd64.tar.gz \
    && mv otelcol /usr/local/bin/ 

# Retrieve python components
FROM python:3.12.4 AS python-retriever

ARG ORCHESTRATOR_VERSION
ARG QUALITYGATE_VERSION

COPY pip.conf /root/.config/pip/pip.conf

RUN pip3 install --no-cache-dir opentf-orchestrator===${ORCHESTRATOR_VERSION}

## Build otf image
FROM python:3.12.4

ARG BUNDLE_VERSION
ARG IMAGE_VERSION
ARG ORCHESTRATOR_VERSION
ARG QUALITYGATE_VERSION
ARG PYTHON_PLUGINS_VERSION
ARG JAVA_PLUGINS_VERSION
ARG ALLURE_VERSION
ARG SCM_VERSION
ARG APT_USER
ARG APT_PWD
ARG APT_REPOSITORY
ARG OTELCOL_VERSION


COPY --from=java-retriever /java-services /app/plugins/java
COPY --from=java-retriever /opt/allure-${ALLURE_VERSION} /opt/allure-commandline
COPY --from=java-retriever /usr/local/bin/otelcol /usr/local/bin

COPY --from=python-retriever /usr/local/lib/python3.12/site-packages /usr/local/lib/python3.12/site-packages
COPY --from=python-retriever /usr/local/bin/opentf-* /usr/local/bin/

COPY otf-image-conf /app
COPY otf-images.json /app/BOM.json

RUN apt-get update \
    && apt-get install -y ca-certificates \
    # Install java
    && wget https://download.java.net/java/GA/jdk21.0.2/f2283984656d49d69e91c558476027ac/13/GPL/openjdk-21.0.2_linux-x64_bin.tar.gz \
    && tar xvf openjdk-21.0.2_linux-x64_bin.tar.gz \
    && chown -R root:root jdk-21.0.2 \
    && mv jdk-21.0.2/ /usr/local/jdk-21 \
    && rm openjdk-21.0.2_linux-x64_bin.tar.gz \
    # Remove cache
    && rm -rf /var/lib/apt/lists

WORKDIR /app

EXPOSE 7774
EXPOSE 7775
EXPOSE 7776
EXPOSE 7796
EXPOSE 9464
EXPOSE 38368
EXPOSE 24368
EXPOSE 12312
EXPOSE 34537

ENV OPENTF_CONFIG=/app/opentf-config.yaml
ENV JAVA_HOME=/usr/local/jdk-21
ENV PATH=${PATH}:/opt/allure-commandline/bin:${JAVA_HOME}/bin
ENV PYTHONUNBUFFERED 1
ENV JAVA_TRUSTSTORE=/app/security/cacerts

ENV JAVA_PLUGINS_VERSION=${JAVA_PLUGINS_VERSION}

RUN mkdir -p /etc/squashtf
RUN mkdir -p /app/insights/css
RUN mkdir -p /app/qualitygates
RUN mkdir -p /app/security && cp /usr/local/jdk-21/lib/security/cacerts /app/security/cacerts

# We need to be able to run keytool as root
RUN ln -s /usr/local/jdk-21/lib/libjli.so /usr/lib
RUN chmod +s /usr/local/jdk-21/bin/keytool

CMD ["python", "/usr/local/lib/python3.12/site-packages/opentf/scripts/startup.py"]

LABEL BUNDLE_VERSION=${BUNDLE_VERSION}
LABEL IMAGE_VERSION=${IMAGE_VERSION}
LABEL ORCHESTRATOR_VERSION=${ORCHESTRATOR_VERSION}
LABEL QUALITYGATE_VERSION=${QUALITYGATE_VERSION}
LABEL PYTHON_PLUGINS_VERSION=${PYTHON_PLUGINS_VERSION}
LABEL ALLURE_VERSION=${ALLURE_VERSION}
LABEL SCM_VERSION=${SCM_VERSION}
LABEL OTELCOL_VERSION=${OTELCOL_VERSION}
